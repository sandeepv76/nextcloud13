<?php 
$OC_Version = array(13,0,7,2);
$OC_VersionString = '13.0.7';
$OC_Edition = '';
$OC_Channel = 'stable';
$OC_VersionCanBeUpgradedFrom = array (
  'nextcloud' => 
  array (
    '12.0' => true,
    '13.0' => true,
  ),
  'owncloud' => 
  array (
  ),
);
$OC_Build = '2018-10-11T07:39:19+00:00 078c1088b6acc34780e3c21135e8208163bb8689';
$vendor = 'nextcloud';

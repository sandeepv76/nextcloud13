<?php
        $user = $this->data['initiator'];
        $textShared = "ha condiviso dei file con te.<br/>shared files with you";
$bodyParts1 = 
<<<EOF
                <!-- 1 Column Text + Button : BEGIN -->
                <tr>
                    <td bgcolor="#ffffff">
                        <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tr>
                                <td style="padding: 40px 40px 20px;">
                                  <h1 style="margin: 0 0 10px 0; font-family: 'Source Sans Pro', sans-serif; font-size: 24px; line-height: 125%; color: #231f20; font-weight: 600; text-align: center;">$user</h1>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 0 40px 40px; font-family: 'Source Sans Pro', sans-serif; font-size: 21px; line-height: 125%; color: #231f20; font-weight: 300; text-align: center;">
                                  <p class="share-text" style="margin: 0;">$textShared </p>
                                </td>
                            </tr>
EOF;

<?php
$footerParts = <<<EOF
                <!-- Footer : BEGIN -->
                <tr>
                    <!-- Bulletproof Background Images c/o https://backgrounds.cm -->
                    <td bgcolor="#1b1d27" valign="middle" style="padding: 40px;">
                      <div style="display:inline-block; margin: 0 -2px; max-width:60%; vertical-align:top; width:100%;" class="stack-column">
                          <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tr>
                                <td style="text-align: center; padding: 0 10px;">
                                </td>
                            </tr>
                            <tr>
                                <td class="td-divider pad-xs" style="text-align: left; font-family: 'Source Sans Pro', sans-serif; font-size: 10px; line-height: 125%; color: #65696b; padding-right: 40px;">
                                   <p style="margin: 0; font-size: 10px; font-weight: 300; line-height: 125%;">Le informazioni contenute in questo messaggio di posta elettronica e/o nel/i file/s
ad esso allegati sono da considerarsi strettamente riservate.
Il loro utilizzo è consentito esclusivamente al destinatario del messaggio, per le
finalità indicate nel messaggio stesso. Qualora riceveste questo messaggio senza
esserne il destinatario, Vi preghiamo cortesemente di darcene notizia via e-mail e
di procedere alla distruzione del messaggio stesso. Ogni altro impiego di esso, ivi
compresa la divulgazione anche parziale ad altri soggetti, è contraria alla legge.</p>

<p style="margin: 10px 0 0 0; font-size: 10px; font-weight: 300; line-height: 125%;">This e-mail contains confidential and/or privileged information.
If you are not the intended recipient, or have received this e-mail in error, please
notify the sender immediately and delete this message. Any unauthorized copying,
disclosure or distribution of the content of this e-mail is strictly forbidden.</p>
                                </td>
                            </tr>
                          </table>
                      </div>
                      <!--[if mso]>
                      </td>
                      <td align="left" valign="top" width="220">
                      <![endif]-->
                      <div style="display:inline-block; margin: 0 -2px; max-width:38.5%; vertical-align:top; width:100%;" class="stack-column stack-column-last">
                          <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tr>
                                <td style="text-align: center; padding: 0 10px;">
                                </td>
                            </tr>
                            <tr>
                                <td class="pad-xs" style="text-align: left; font-family: 'Source Sans Pro', sans-serif; font-size: 14px; line-height: 120%; color: #65696b; padding-left: 40px;">
                                    <p style="margin: 0; font-size: 14px; font-weight:bold; line-height: 120%">BERLONI BAGNO srl</p>
                                    <p style="margin: 0 0 10px; font-size: 12px; font-weight: 300; line-height: 120%">Mobili per ambiente bagno,<br/>
arredobagno di design<br/>
in Italia e nel mondo.</p>
<p style="margin: 0 0 10px; font-size: 12px; font-weight: 300; line-height: 120%"><span class="contact">61034 Fossombrone (PU)</span><br/>
Via G. di Vittorio, 1<br/>
T <span class="contact">+ 39 0721725523</span><br/>
P.IVA IT 00425040417</p>
<p style="margin: 0; font-size: 12px; font-weight: 300; line-height: 120%"><span class="contact">info@berlonibagno.com</span><br/>
<span class="contact">www.berlonibagno.com</span></p>
                                </td>
                            </tr>
                          </table>
                      </div>

                    </td>
                </tr>
                <!-- Footer : END -->

                <!-- Clear Spacer : BEGIN -->
                <tr class="hide-xs">
                    <td aria-hidden="true" height="40" style="font-size: 0; line-height: 0;">
                        &nbsp;
                    </td>
                </tr>
                <!-- Clear Spacer : END -->

        </table>
        <!-- Table : END -->

      </div>

    </center>
</table>
<!-- Body : END -->

EOF;


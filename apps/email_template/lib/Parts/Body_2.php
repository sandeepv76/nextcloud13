<?php
$fileName = $this->data['filename'];
$expiration = $this->data['expiration'];
$link = $this->data['link'];
$textExpired = '';

if ( isset( $expiration ) ) {
    $textExpired = '<p style="margin: 0 0 40px; color: #231f20; font-weight: 300;">La condivisione scadrà il'.$expiration.'.<br/>Share will expire on'.$expiration.'.</p>';
} 

$bodyParts2 =
<<<EOF
                            <tr>
                                <td style="padding: 0 40px 40px; font-family: 'Source Sans Pro', sans-serif; font-size: 21px; line-height: 125%; color: #231f20; font-weight: 600; text-align: center; text-transform:uppercase;">
                                  <p style="margin: 0;">$fileName</p>
                                </td>
                            </tr>
                            <tr>
                              <td style="padding: 0 40px; font-family: 'Source Sans Pro', sans-serif; font-size: 15px; line-height: 140%; color: #231f20; text-align: center;">
                                $textExpired
                                <!-- Button : BEGIN -->
                                    <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" style="margin: auto;">
                                        <tr>
                                            <td style="border-radius: 15px; background: #68a5ae; text-align: center;" class="button-td">
                                              <a href="$link" style="background: #68a5ae; border: 15px solid #68a5ae; font-family: 'Source Sans Pro', sans-serif; font-size: 16px; line-height: 110%; text-align: center; text-decoration: none; display: block; border-radius: 15px; font-weight: 600;" class="button-a">
                                                  <span style="color:#ffffff; padding-left: 15px; padding-right: 15px;" class="button-link">Download</span>
                                              </a>
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- Button : END -->
                                </td>
                            </tr>
                            <tr>
                                  <td style="padding: 40px;">
                                    <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" style="margin: auto; width: 100%;">
                                      <tr>
                                        <td style="border-bottom: 2px solid #918f90;">
                                        </td>
                                      </tr>
                                    </table>
                                  </td>
                            </tr>
                            <tr>
                              <td>
                                  <div>
                                      <table role="presentation" border="0" cellpadding="0" cellspacing="0" align="center" width="100%" style="max-width:400px; margin: auto;">
                                          <tr>
                                            <td style="padding-bottom:  40px; font-family: 'Source Sans Pro', sans-serif; font-size: 15px; line-height: 140%; color: #231f20; text-align: center;">
                                                <h2 style="margin: 0 0 10px 0; font-family: 'Source Sans Pro', sans-serif; font-size: 18px; line-height: 125%; color: #231f20; font-weight: 600;">MESSAGGIO / MESSAGE</h2>
                                                <p style="margin: 0; color: #231f20; font-weight: 300"></p>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <!-- 1 Column Text + Button : END -->
EOF;

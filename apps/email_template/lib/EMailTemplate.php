<?php
/**
 * @copyright 2018, Marius Blüm <marius@nextcloud.com>
 *
 * @author Marius Blüm <marius@nextcloud.com>
 *
 * @license GNU AGPL version 3 or any later version
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
namespace OCA\EmailTemplate;

use OC\Mail\EMailTemplate as ParentTemplate;

class EMailTemplate extends ParentTemplate {

/* HEAD */

	public function addHeader() {
		if ($this->headerAdded) {
			return;
		}
		$this->headerAdded = true;

		include_once ('Parts/Style.php');
        include_once ('Parts/Header.php');

		$this->htmlBody .= $styleParts;
        $this->htmlBody .= $headParts;

	}
	/**
	 * Adds a heading to the email
	 *
	 * @param string $title
	 * @param string|bool $plainTitle Title that is used in the plain text email
	 *   if empty the $title is used, if false none will be used
	 */
	public function addHeading($title='', $plainTitle = '') {
		if ($this->footerAdded) {
			return;
		}

        include_once ('Parts/Body_1.php');

		$this->htmlBody .= $bodyParts1;
	}


	/**
	 * Adds a paragraph to the body of the email
	 *
	 * @param string $text
	 * @param string|bool $plainText Text that is used in the plain text email
	 *   if empty the $text is used, if false none will be used
	 */
	public function addBodyText($text, $plainText = '') {
		if ($this->footerAdded) {
			return;
		}

        include_once ('Parts/Body_2.php');
/*****
******
    DEBUG
*****
*****
ob_start();
print_r($this->data);
$this->htmlBody .=  ob_get_clean();
ob_get_clean();
****/

		$this->htmlBody .= $bodyParts2;
	}

	public function addBodyListItem($text, $metaInfo = '', $icon = '', $plainText = '', $plainMetaInfo = '') {return false;}
	public function addBodyButtonGroup($textLeft,
									   $urlLeft,
									   $textRight,
									   $urlRight,
									   $plainTextLeft = '',
									   $plainTextRight = '') {return false;}
	public function addBodyButton($text, $url, $plainText = '') {return false;}

	/**
	 * Adds a logo and a text to the footer. <br> in the text will be replaced by new lines in the plain text email
	 *
	 * This method completely overwrites the default behaviour.
	 */
	public function addFooter($text = '') {
		if ($this->footerAdded) {
			return;
		}
		$this->footerAdded = true;
		$this->ensureBodyIsClosed();

        include_once ('Parts/Footer.php');

		$this->htmlBody .= $footerParts;
		$this->htmlBody .= $this->tail;
		$this->plainBody .= PHP_EOL . '-- ' . PHP_EOL;
		$this->plainBody .= str_replace('<br>', PHP_EOL, $text);
	}
}
